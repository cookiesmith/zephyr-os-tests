# Overview

This project is a sample of zephyr OS application implementing various of embeded
software problems: interrupt handling, parallelism, concurrency, etc.

The used board was STM32 Nucleo g474RET6U.

The project requirements are:

- Something that uses Z-Bus

- Implement Shell, with a "return_running_tasks", "busy_stack", and "runtime_stats"
  fucntions

- Keyboard management with at least one key (interrupt handler, debounce and key
  queue)

- A LED indicating system activity, using softtimer

- Verificar o sistema de print do RTOS (utilizando tarefa para desacoplar os logs do
  sistema?) (sistema de debug por print)

- Implement at least two shell commands: Signal generation through DAC, and print
  it's harmonics.

## Compiling

```
./deploy.sh all # Compiles and flash the program
./deploy.sh [help] # See other possible commands
```

## Debbugging

```
# Start openocd server
./deploy.sh server_start;

# Make sure we have port permission
sudo chmod 777 /dev/ttyACM0;

# Attach debugger to openocd server
arm-none-eabi-gdb -b 115200 --tty=/dev/ttyACM0 build/zephyr/zephyr.elf;

# In another window, you might want to attach to serial terminal
screen /dev/ttyACM0 115200
```

## Requirements

The board hardware must have a push button connected via a GPIO pin. These are
called "User buttons" on many of Zephyr's :ref:`boards`.

# Important

The button must be configured using the ``sw0`` :ref:`devicetree <dt-guide>`
alias, usually in the :ref:`BOARD.dts file <devicetree-in-out-files>`. You will
see this error if you try to build this sample for an unsupported board:

.. code-block:: none

   Unsupported board: sw0 devicetree alias is not defined

You may see additional build errors if the ``sw0`` alias exists, but is not
properly defined.

The sample additionally supports an optional ``led0`` devicetree alias. This is
the same alias used by the :zephyr:code-sample:`blinky` sample. If this is provided, the LED
will be turned on when the button is pressed, and turned off off when it is
released.

## Devicetree

This section provides more details on devicetree configuration.

Here is a minimal devicetree fragment which supports this sample. This only
includes a ``sw0`` alias; the optional ``led0`` alias is left out for
simplicity.

.. code-block:: devicetree

   / {
   	aliases {
   		sw0 = &button0;
   	};

   	soc {
   		gpio0: gpio@0 {
   			status = "okay";
   			gpio-controller;
   			#gpio-cells = <2>;
   			/* ... */
   		};
   	};

   	buttons {
   		compatible = "gpio-keys";
   		button0: button_0 {
   			gpios = < &gpio0 PIN FLAGS >;
   			label = "User button";
   		};
   		/* ... other buttons ... */
   	};
   };

As shown, the ``sw0`` devicetree alias must point to a child node of a node
with a "gpio-keys" :ref:`compatible <dt-important-props>`.

The above situation is for the common case where:

- ``gpio0`` is an example node label referring to a GPIO controller
-  ``PIN`` should be a pin number, like ``8`` or ``0``
- ``FLAGS`` should be a logical OR of :ref:`GPIO configuration flags <gpio_api>`
  meant to apply to the button, such as ``(GPIO_PULL_UP | GPIO_ACTIVE_LOW)``

This assumes the common case, where ``#gpio-cells = <2>`` in the ``gpio0``
node, and that the GPIO controller's devicetree binding names those two cells
"pin" and "flags" like so:

.. code-block:: yaml

   gpio-cells:
     - pin
     - flags

This sample requires a ``pin`` cell in the ``gpios`` property. The ``flags``
cell is optional, however, and the sample still works if the GPIO cells
do not contain ``flags``.

After startup, the program looks up a predefined GPIO device, and configures the
pin in input mode, enabling interrupt generation on falling edge. During each
iteration of the main loop, the state of GPIO line is monitored and printed to
the serial console. When the input button gets pressed, the interrupt handler
will print an information about this event along with its timestamp.


# Resources

[Brief debug tutorial](https://elrobotista.com/en/posts/stm32-debug-linux/)

[Nucleo G474RE schematics](https://www.st.com/content/ccc/resource/technical/layouts_and_diagrams/schematic_pack/group1/69/2a/24/ce/d1/1b/45/21/mb1367-g474re-c04_schematic/files/mb1367-g474re-c04_schematic.pdf/jcr:content/translations/en.mb1367-g474re-c04_schematic.pdf)

[ELF section analisys](https://jonathanhamberg.com/post/2018-10-03-decreasing-elf-size/)

[ELF sections analisys (suplemental commands)](https://www.baeldung.com/linux/file-elf-extract-raw-contents)

