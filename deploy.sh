#!/bin/bash

# Current directory (used as build dir)
ROOT_DIR=$(pwd)

# Error codes
OK=0
NO_SERVER=1

unset OPENOCD
export OPENOCD="$(pidof openocd)";

finish()
{
	return $ERROR
}

help()
{
	echo -e "\
deploy.sh - simple script to send lightning binary and making tests\n\
	help - shows this screen;\n\

	send - sends the binary. You must run \"./deploy.sh compile\" first;\n\

	execute [COMMAND] - Runs COMMAND through netcat to openocd server. You must run \"./deploy.sh server_start\" first

	server_start - Starts a openocd server. Needed before flashing or debugging

	server_check - Check if openocd server is running, returning it's pid if true

	server_stop - Stops any running openocd server

	compile - Compile the source in current directory

	all - Compile, start server, and flash the built source into stm32-nucleo

	setup - Tries to install dependencies, west and create a venv (tested only in ArchLinux)
	
	Any other system command will works as well (e.g. \"./deploy.sh west update\")
	";
	exit
}

# If openocd is running, runs given command
execute()
{
	if [ -z "$OPENOCD" ]; then
		echo "Openocd server didn't start!" >&2;
		return $NO_SERVER
	fi

	echo "$1; exit;" | nc localhost 4444;
	sleep 3
	return $?
}


server_reset()
{
	if [ -n "$OPENOCD" ]; then
		kill $OPENOCD
	fi
	openocd -f openocd.cfg &
	return $OK
}

# Stops open openocd server
server_stop()
{
	if [ -n "$OPENOCD" ]; then
		kill $OPENOCD
	fi
	return $OK
}

# Check if openocd server is running, returning it's pid
server_status()
{
	if [ -n "$OPENOCD" ]; then
		echo -e "Openocd is running; PID is $OPENOCD";
	fi
	return 0;
}

# Flashes the built binary to the mc
send()
{
	COMMAND[0]="reset halt"
	COMMAND[1]="stm32l4x mass_erase 0"
	COMMAND[2]="flash write_bank 0 $(pwd)/$(find . -iname "zephyr.bin") 0"

	commands=0
	while [ $commands -lt ${#COMMAND[@]} ]; do
		execute "${COMMAND[$commands]}"
		ERROR=$?
		if [ "$ERROR" -ne 0 ]; then
			return $ERROR;
		fi
		commands=$(( $commands+1 ))
	done

	return $ERROR
}

# Starts openocd server and deploy to background
server_start()
{
	if [ -z "$OPENOCD" ]; then
		openocd -f openocd.cfg &
	else
		echo "Openocd is already running with pid $OPENOCD)"
		echo "You can use \"./deploy.sh server_stop\" to stop the server"
		return 1;
	fi

	echo "Openocd is running; PID is $OPENOCD"

	# Get openocd pid set for the environment
	unset OPENOCD
	export OPENOCD="$(pidof openocd)";

	return 0;
}

# For compiling the source
compile()
{
	west build -o="-j$(nproc)" -p always -b nucleo_g474re "$1" -d build;
}

# Compile, start openocd server and flash program
all()
{
	compile;
	server_start;
	send;
	echo "Done. You might want to debug the application."
	echo;
	echo "arm-none-eabi-gdb -b 115200 --tty=$(find /dev -iname '*ttyACM*') ./build/zephyr/zephyr.elf" -ex \"target extended-remote :3333\"
	echo;
	echo "screen $(find /dev -iname '*ttyACM*') 115200"
	echo;
	echo "Don't forget to double check baudrate and /dev port."
}

# Setup dependencies, west and venv for this machine
setup()
{
	# FUNCTION NOT ENTIRELLY TESTED

	# This here is optional. For a nice gdb interface
	# mv -f ~/.gdbinit ~/gdbinit.old
	# wget -P ~ https://git.io/.gdbinit
	# pip install pygments
	
	# "pacman" commands by your own package manager
	
	# Update system
	sudo pacman --noconfirm -Syu

	# Install dependencies
	for i in \
		git cmake ninja-build gperf \
 		ccache dfu-util device-tree-compiler wget \
		python3-dev python3-pip python3-setuptools python3-tk python3-wheel xz-utils file \
		make gcc gcc-multilib g++-multilib libsdl2-dev libmagic1 \
	; do
		sudo pacman --noconfirm -S $i;
	done

	# Create venv
	python3 -m .venv .venv
	# Activate
	source .venv/bin/activate
	# install west
	pip install west

	# Init project folder
	west init .
	# Update west
	west update
	# Export zephyr to cmake
	west zephyr-export
	# Install final dependencies
	pip install -r zephyr/scripts/requirements.txt
}

# Things to do before building or sending
pre()
{
	ZEPHYR_ENV=~/zephyr
	# - create symbolic links to allow building locally
	for i in bootloader modules tools zephyr .west .venv; do
		# If there's an sl already, remove before creating
		if [ -h ./$i ]; then
			rm -r "./$i";
		fi
		ln -s $ZEPHYR_ENV/$i .;
	done
	# - source venv
	source ./.venv/bin/activate
}

# Thins to do after building or sending
post()
{
	# - clear created symlinks
	rm bootloader modules tools zephyr .west .venv;
}


if [ $# -lt 1 ]; then
	help
fi

pre
COMMAND=$1
shift 1;
$COMMAND $@
#post
