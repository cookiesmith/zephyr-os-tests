// Libraries
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/sys/printk.h>
#include <zephyr/sys/__assert.h>
#include <zephyr/sys/util.h>
#include <string.h>

/* Abreviations
 * dv - device-tree
 * sw/bt - Switch, or button (is the samw thing)
 *
 */

#define SLEEP_TIME_MS	200

#define ZEPHYR_USER_NODE DT_PATH(zephyr_user)

/* size of stack area used by each thread */
#define STACKSIZE 1024

/* scheduling priority used by each thread */
#define PRIORITY 7

// Default sleep time in miliseconds
#define SLEEP_TIME_MS	200 

// Aliasses definition
// Button
#define SW1_NODE	DT_ALIAS(sw1)
// Led
#define LED2_NODE	DT_ALIAS(led2)

// Aliasses check
// Led
#if !DT_NODE_HAS_STATUS(LED2_NODE, okay)
	#error "led2 devicetree alias is not defined. See *.overlay"
#endif
// Button
#if !DT_NODE_HAS_STATUS(SW1_NODE, okay)
	#error "sw1 devicetree alias is not defined. See *.overlay"
#endif

// Devices structure
struct dev
{
    struct gpio_dt_spec spec;
    uint8_t num;
};

// Led declaration
static const struct dev led2 = {
    .spec = GPIO_DT_SPEC_GET_OR(LED2_NODE, gpios, {0}),
    .num = 2,
};
// button declaration
static const struct dev sw1 = {
    .spec = GPIO_DT_SPEC_GET_OR(SW1_NODE, gpios, {0}),
    .num = 1,
};

struct printk_data_t {
	void *fifo_reserved; /* 1st word reserved for use by fifo */
	uint32_t led;
	uint32_t cnt;
};

K_FIFO_DEFINE(printk_fifo);

/* Function to setup led as output */ 
void dev_init(const struct dev *dev, char input) 
{ 
	static uint32_t ret = 0;
	const struct gpio_dt_spec *spec = &dev->spec; 

	// Get led specification structure for control
	if (!device_is_ready(spec->port))
	{ 
		printk("Error: %s device is not ready\n", spec->port->name);
		//return 1; 
	} 

	if (input)
	{
		ret = gpio_pin_configure_dt(spec, GPIO_INPUT); 
	}
	else
	{
		ret = gpio_pin_configure_dt(spec, GPIO_OUTPUT); 
	}
		
	if (ret)
	{ 
		#ifdef DEBUG
			printk("Error %d: failed to configure pin %d (%s '%d')\n", ret, spec->pin, spec->port->name, dev->num);
		#endif
		//return ret; 
	}
}
void blink(const struct dev *dev, uint32_t sleep_ms, uint32_t id)
{
	//static uint32_t ret, cnt = 0;
	uint32_t cnt = 0;

	const struct gpio_dt_spec *spec = &dev->spec;
	while(1)
	{
		#ifdef DEBUG
			printk("Nothing to be done");
		#endif
		gpio_pin_set(spec->port, spec->pin, cnt % 2);

		struct printk_data_t tx_data = { .led = id, .cnt = cnt };
		size_t size = sizeof(struct printk_data_t);
		char *mem_ptr = k_malloc(size);
        __ASSERT_NO_MSG(mem_ptr != 0);

        memcpy(mem_ptr, &tx_data, size);

        k_fifo_put(&printk_fifo, mem_ptr);

		cnt++;
		k_msleep(sleep_ms);
	//return cnt;
	}
}

void blink0(void)
{
	dev_init(&led2, 0);
	//dev_init(&sw1, 1);
	blink(&led2, 1000, 0);
}

void blink1(void)
{
	//dev_init(&led2, 0);
	//dev_init(&sw1, 1);
	blink(&led2, 1500, 0);
}


void uart_out(void)
{
	while (1) {
		struct printk_data_t *rx_data = k_fifo_get(&printk_fifo, K_FOREVER);
		printk("Toggled led%d; counter=%d\n",
		       rx_data->led, rx_data->cnt);
		k_free(rx_data);
	}
}

K_THREAD_DEFINE(blink0_id, STACKSIZE, blink0, NULL, NULL, NULL,PRIORITY , 0, 0);
K_THREAD_DEFINE(blink1_id, STACKSIZE, blink1, NULL, NULL, NULL,PRIORITY , 0, 0);
K_THREAD_DEFINE(uart_out_id, STACKSIZE, uart_out, NULL, NULL, NULL, PRIORITY, 0, 0);

//int main(void)
//{
//	return 0;
//}
